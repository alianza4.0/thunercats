#+++ Configuracion Acceso Servers AD********************************************************

# Nombres de las máquinas
$VMADpadre = "AD-Thunder"
$VMADhijo = "AD-SubThunder"
$VMwin10 = "WinThunder"

# Nombres de usuarios

# Default de servers
$defaultAdmin = "Administrator" #Nombre del usuario default

# Nombres nuevos post AD
$userADdominio = "THUNDER\Administrator" # Nombre del usuario de la máquina cliente VM
$userADsubdominio = "SUBTHUNDER\Administrator" # Nombre del usuario de la máquina server VM

#Contraseña
$pass = ConvertTo-SecureString -String "hola123.," -AsPlainText -Force

# Creacion de credenciales por Virtual Machine Hyper-V pre config (preAD)
$credentialADpadre = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $defaultAdmin, $pass
$credentialADhijo = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $defaultAdmin, $pass
$credentialWin10 = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $defaultAdmin, $pass

# Creacion de sesiones
$sesionADpadre = New-PSSession -VMName $VMADpadre -Credential $credentialADpadre
$sesionADhijo = New-PSSession -VMName $VMADhijo -Credential $credentialADhijo
$sesionServer = New-PSSession -VMName $VMwin10 -Credential $credentialWin10

# Ingresar a sesion
#----------------------------------Para acceder, use alguna de las siguientes lineas-----------------------------------
# Del padre pre AD
Enter-PSSession -Session $sesionADpadre

# Del hijo pre AD
Enter-PSSession -Session $sesionADhijo

# Del Win10
Enter-PSSession -Session $sesionADpadre