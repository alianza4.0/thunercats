

#Configuración de DHCPAntes que nada se debe correr el comandos
#config y se deben modificar la opciones 2 y 8, dc01 y 10.0.63.1 máscara
#255.255.192.0 y establcer DNS en loopback 127.0.0.1 respectivamente. 

#En este dominio de core, se deberá instalar un servidor DHCP extra de 
#alta disponibilidad, para eso debemos instalar los siguiente paquetes 
#en los dos servidores:

Install-WindowsFeature -Name DHCP
Install-WindowsFeature -Name RST
Install-WindowsFeature -Name DNS

#Unimos el nuevo servidor al que va asignar direcciones, después de esto 
#se debe reiniciar la computadora.
#Comando de entrada:
Add−Computer THUNDERCATS.LOCAL
#se deben crear los usuarios para DHCP y los grupos de seguridad y agregarlos, 
#después reiniciamos el servicio en el equipo.
#Comando de entrada:
netsh dhcp add securitygroups 
Restart-Service dhcpserver6

#A continuación debemos autorizar y verificar la autorización del servidor 
#DHCP en Active Directory, esto permitirá que elservidor DHCP opere en el 
#dominio asignado.
#Comando de entrada:
Add−DhcpServerInDC −DnsName dc01.core.dhcp −IPAddress 10.0.63.2
Get−DhcpServerInDC

#Se configuran los datos específicos necesarios para la organización, como 
#rango de iP’s que deben ser asignadas,iP’s reservadas, el default gateway, 
#la iP del DNS y el nombre del dominio del DNS 
Add−DhcpServerv4Scope -name "core" `
−StartRange 10.0.60.1 `
−EndRange 10.0.60.50 `
−SubnetMask 255.255.192.0 `
−State Active `
Set-DhcpServerv4OptionValue −OptionID 3`
−Value 10.0.0.1 `
−ScopeID 10.0.0.0 `
−ComputerName dc01.core.local `
Set-DhcpServerv4OptionValue −DnsDomain thundercats.local `
−DnsServer 10.0.9.20

#Verificamos la funcionalidad del servidor revisando la configuración de 
#los clientes asignados a thunder-cats.local, ingresamos a cualquier cliente 
#y dentro de las opciones del adapatador de red, seleccionamos iPv4y seleccionamos 
#la opción para que asigne la ip automaticamente a través de un DHCP. 
#Abrimos un promptshell con privilegios de administrador y ejecutamos el siguiente 
#comando:
ipconfig /renew

#Esto nos debe regresar la siguiente información con una iP en el rango que 
#especificamos en el DHCP
#Añadimos el segundo DHCP al primero con el siguiente comando:
Add−DnsServerConditionalForwarderZone -Name "dc02.failOver.dhcp" `
−MasterServer 10.0.63.2

#DHCP de Alta Disponibilidad
#Se hizo un segundo controlador de dominio y se instalo el DHCP, al controlador 
#de dominio se le puso dc02.failover.dhcpy se configuró un conditional forwarder 
#para que se pudiera conectar al core dhcp.
Add−DnsServerConditionalForwarderZone −Name "dc01.core.dhcp" `
−MasterServer 10.0.63.2

#Se configuró en el dhcp core que las cargas se repartieran 50-50
Add−DhcpSrverv4FailOver −Name Core-FailOver `
−PartnerServer dc02.failOver.dhcp −ScopeId 10.0.0.0 } `
−LoadBalancePercent 50

#Para ver que direcciones IP ha asignado cada servidor se usa el comando:
Get−DhcpServerv4Lease −ComputerName dc01.core.dhcp 
Get−DhcpServerv4Lease −ComputerName dc02.failOver.dhcp