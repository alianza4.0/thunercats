#Instalar Active Directory
install-windowsfeature AD-Domain-Services -IncludeManagementTools

#ipconfig
New-NetIPAddress -InterfaceAlias "Ethernet" -IPAddress 10.0.20.2 -PrefixLength 18 
Set-DnsClientServerAddress -InterfaceAlias "Ethernet" -ServerAddresses ("10.0.20.1")

#Subdominio thundera.thundercats.local
install-Addsdomain -domaintype "childDomain" -parentdomainname "thundercats.local" -newdomainname "thundera" -credential ( credential ) -NoRebootOnCompletion:$false
