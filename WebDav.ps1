﻿Invoke-Command  -VMName "TERCERPLANETAPC" -ScriptBlock {
    Install-WindowsFeature -Name Web-Server -IncludeManagementTools
    Install-WindowsFeature -Name FS-Resource-Manager -IncludeManagementTools
    Install-WindowsFeature -Name Web-Http-Redirect,web-dav-publishing,Web-Windows-Auth,Web-Url-Auth,web-mgmt-console,web-ip-security,web-basic-auth,Web-Client-Auth,web-certprovider,web-digest-Auth,web-cert-auth


    Add-DnsServerResourceRecordA -Name "files" -ZoneName "tercerplaneta.local" -AllowUpdateAny -IPv4Address "10.0.21.1" 
    cd IIS:\
    #Creacion de site moes
    $rutaDelSitio = "C:\webdav"             
    $appPool = "webdav"                  
    $NombreDelSitio = "files"                        
    $NombreCompletoSitio = "files.tercerplaneta.local"            

    New-Item $rutaDelSitio -type Directory
    #Set-Content $rutaDelSitio\Default.htm "<h1>Bienvenido a Moe's</h1>"
    New-Item IIS:\AppPools\$appPool
    New-Item IIS:\Sites\$NombreDelSitio -physicalPath $rutaDelSitio -bindings @{protocol="http";bindingInformation=":80:"+$NombreCompletoSitio} 
    Set-ItemProperty IIS:\Sites\$NombreDelSitio -name applicationPool -value $appPool
    Start-Website -Name $NombreDelSitio

    New-WebVirtualDirectory -Site "files" -Name WebDav -PhysicalPath C:\Webdav
    C:\Windows\System32\inetsrv\appcmd.exe set config "files/WebDav" -section:system.webServer/webdav/authoringRules /+"[users='*',path='*',access='Read, Write']" /commit:apphost
    C:\Windows\System32\inetsrv\appcmd.exe set config "files" -section:system.webServer/directoryBrowse /enabled:"True" /showFlags:"Date, Time, Size, Extension"


    New-Item C:\Webdav\EnterpriseD -type Directory
    New-WebVirtualDirectory -Site "files" -Name Enterprise -PhysicalPath C:\Webdav\EnterpriseD
    C:\Windows\System32\inetsrv\appcmd.exe set config "files/EnterpriseD" -section:system.webServer/webdav/authoringRules /+"[roles='Enterprise Admis',path='*',access='Read, Write']" /commit:apphost

    New-Item C:\Webdav\DomainA -type Directory
    New-WebVirtualDirectory -Site "files" -Name DomainA  -PhysicalPath C:\Webdav\DomainA
    C:\Windows\System32\inetsrv\appcmd.exe set config "files/DomainA" -section:system.webServer/webdav/authoringRules /+"[roles='Domain Admis',path='*',access='Read, Write']" /commit:apphost

    New-Item C:\Webdav\DomainU -type Directory
    New-WebVirtualDirectory -Site "files" -Name DomainA  -PhysicalPath C:\Webdav\DomainU
    C:\Windows\System32\inetsrv\appcmd.exe set config "files/DomainU" -section:system.webServer/webdav/authoringRules /+"[roles='Domain User',path='*',access='Read, Write']" /commit:apphost

    New-FsrmQuota -Path "C:\Webdav\DomainA" -Size 100MB
    New-FsrmQuota -Path "C:\Webdav\EnterpriseD" -Size 100MB
    New-FsrmQuota -Path "C:\Webdav\DomainU" -Size 10MB
    #cd C:\Windows\
}