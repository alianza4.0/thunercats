# Variables
$parentDomainName = "Thundercats.local"
$childDomainName = "Thundera"
$childDomainNetbiosName = "SUBTHUNDER"

#===============================================================
#=======================InstalarRoles===========================
#Obtener los roles disponibles
# Get-WindowsFeature

#De la lista, instalamos el rol de Active Directory Domain Services y el de DNS

Install-WindowsFeature -Name AD-Domain-Services -IncludeManagementTools
Install-WindowsFeature DNS -IncludeManagementTools

#De la lista, instalamos el rol de Active Directory Domain Services y el de DNS

Get-Command -Module ADDSDeployment
#Instalamos las opciones que nos aparecen de la siguiente manera:

Install -ADDSDomain
-CreateDnsDelegation:$True `
 -DatabasePath "C:\Windows\NTDS" `
 -DomainMode "Win2012R2" `
 -domaintype “childdomain” `
-parentdomainname $parentDomainName `
-newdomainname $childDomainName `
 -DomainNetbiosName $childDomainNetbiosName `
 -ForestMode "Win2012" `
 -InstallDns:$true `
 -LogPath "C:\Windows\NTDS" `
 -NoRebootOnCompletion:$false `
 -SysvolPath "C:\Windows\SYSVOL" `
 -Force:$true
