﻿Install-WindowsFeature -Name ADCS-Cert-Authority -IncludeManagementTools
Install-WindowsFeature ADCS-Web-Enrollment
Install-AdcsCertificationAuthority -CAType EnterpriseSubordinateCA -CACommonName "CASubordinada" -KeyLength 2048 -HashAlgorithmName SHA256 -CryptoProviderName "RSA#Microsoft Software Key Storage Provider"

#Después de obtener el .req, se le manda el archivo a la CA Root linux.
#Éste lo firma, y nos devuelve el .crt y adicionalmente, tenemos que agregar el .pem de la CA Root para nuestra CA
#subordinada sepa quién es la CA Root.

$pathPem = "C:\certs\cacert.pem" #Ruta donde está el .pem de la CA Root <CAMBIAR>
$pathCert = "C:\certs\subordinada.test.local_CASubordinada.crt" #Ruta donde está el certificado ya firmado <CAMBIAR>


#Se instala el .pem dentro de la CA Subordinada.
Import-Certificate -FilePath $pathPem -CertStoreLocation 'Cert:\LocalMachine\Root' -Verbose


#Se agrega el .crt firmado por la CA Root dentro de la subordinada. 
#AUN NO FUNCIONA EL COMANDO DE ABAJO#
#Import-Certificate -FilePath $pathCert -CertStoreLocation 'Cert:\LocalMachine\CA' -Verbose

# <Duplicacion de un certificado>

#=============================Duplicate Certificates=======================
$ConfigContext = ([ADSI]"LDAP://RootDSE").ConfigurationNamingContext 
$ADSI = [ADSI]"LDAP://CN=Certificate Templates,CN=Public Key Services,CN=Services,$ConfigContext" 
$templatePath = "LDAP://CN=Aliens,CN=Certificate Templates,CN=Public Key Services,CN=Services,CN=Configuration,DC=Thundercats,DC=local"

#Estos son para los valores shidos
$NewTempl = $ADSI.Create("pKICertificateTemplate", "CN=Aliens")  
$NewTempl.put("distinguishedName","CN=Aliens,CN=Certificate Templates,CN=Public Key Services,CN=Services,$ConfigContext")

# and put other atributes that you need 
#(New-Object DirectoryServices.DirectorySearcher “ObjectClass=user”).FindAll() | Select $templatePath

$NewTempl.put("flags","131640")
$NewTempl.put("displayName","Aliens")
$NewTempl.put("revision","100")
$NewTempl.put("pKIDefaultKeySpec","1")

$NewTempl.SetInfo()

$NewTempl.put("pKIMaxIssuingDepth","0")
$NewTempl.put("pKICriticalExtensions","2.5.29.15")
$NewTempl.put("pKIExtendedKeyUsage","1.3.6.1.4.1.311.10.3.4, 1.3.6.1.5.5.7.3.2")
#$NewTempl.put("pKIDefaultCSPs","1,Microsoft RSA SChannel Cryptographic Provider")
$NewTempl.put("msPKI-RA-Signature","0")
$NewTempl.put("msPKI-Enrollment-Flag","41")
$NewTempl.put("msPKI-Private-Key-Flag","100925456")
$NewTempl.put("msPKI-Certificate-Name-Flag","-1979711488")
$NewTempl.put("msPKI-Minimal-Key-Size","2048")
$NewTempl.put("msPKI-Template-Schema-Version","2")
$NewTempl.put("msPKI-Template-Minor-Revision","4")
$NewTempl.put("msPKI-Cert-Template-OID","1.3.6.1.4.1.311.21.8.14204780.2945395.5786293.11224635.12515457.116.6314294.1068 143")
$NewTempl.put("msPKI-Certificate-Application-Policy","1.3.6.1.4.1.311.10.3.4, 1.3.6.1.5.5.7.3.2")

$NewTempl.SetInfo()

$WATempl = $ADSI.psbase.children | where {$_.displayName -match "Basic EFS"}

$NewTempl.pKIKeyUsage = $WATempl.pKIKeyUsage
$NewTempl.pKIExpirationPeriod = $WATempl.pKIExpirationPeriod
$NewTempl.pKIOverlapPeriod = $WATempl.pKIOverlapPeriod
$NewTempl.SetInfo()

"Se creo el certificado Aliens con Exito..."
$NewTempl | select *

Add-CATemplate -Name "Aliens" #Agregar la plantilla dentro de las plantillas aplicables.