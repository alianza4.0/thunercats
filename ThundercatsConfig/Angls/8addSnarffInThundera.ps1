﻿# Mostrar usuarios del AD
Get-ADUser -Filter * | Ft

#Creamos pass de Snarff
$pass = ConvertTo-SecureString -String "hola123.," -AsPlainText -Force

New-ADUser -Name "Snarff" -AccountPassword $pass -DisplayName "Snarff" `
-Enabled $True -GivenName "Snarff" -Server "Thundera.Thundercats.local" `
-UserPrincipalName "snarff@Thundera.Thundercats.local"