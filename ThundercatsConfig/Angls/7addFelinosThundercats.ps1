﻿# Creamos los integrantes de Felino en Thundercats.local
$pass = ConvertTo-SecureString -String "hola123.," -AsPlainText -Force

New-ADUser -Name "Michi" -AccountPassword $pass -DisplayName "Michi" `
-Enabled $True -GivenName "Michi" -Server "Thundercats.local" `
-UserPrincipalName "Michi@Thundercats.local"

New-ADUser -Name "Mishi" -AccountPassword $pass -DisplayName "Mishi" `
-Enabled $True -GivenName "Mishi" -Server "Thundercats.local" `
-UserPrincipalName "Mishi@Thundercats.local"


# Crear un nuevo grupo en el subdominio
New-ADGroup -Name "Felinos" -GroupCategory Security `
-GroupScope Global -DisplayName "Felinos" -Description "Cats Only"

Add-ADGroupMember -Identity "Felinos" -Members Michi, Mishi
