﻿#===============================Actualiza credenciales===============================================0
# Nombres nuevos post AD
$userADdominio = "THUNDER\Administrator" # Nombre del usuario de la máquina cliente VM
$userADsubdominio = "SUBTHUNDER\Administrator" # Nombre del usuario de la máquina server VM

#Creamos las nuevas credenciales post AD
$credentialADpadre = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $userADdominio, $pass
$credentialADhijo = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $userADsubdominio, $pass

# Creacion de nuevas sesiones
$sesionADpadre = New-PSSession -VMName $VMADpadre -Credential $credentialADpadre
$sesionADhijo = New-PSSession -VMName $VMADhijo -Credential $credentialADhijo

# Ingresar a sesion
# Del padre pre AD
Enter-PSSession -Session $sesionADpadre

# Del hijo pre AD
Enter-PSSession -Session $sesionADhijo