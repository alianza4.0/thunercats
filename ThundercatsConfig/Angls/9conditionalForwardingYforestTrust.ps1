﻿# Variables
$destinationForest = "10.0.1.1"
$strRemoteForest = "TercerPlaneta.local"
$strRemoteAdmin = "TERCERPLANETA\Administrator"
$strRemoteAdminPassword = "hola123.,"

#========================================================================
#=========================AñadirConditionalForwarder=====================
#Añadimos el forwarding
Add-DnsServerConditionalForwarderZone -Name $strRemoteForest `
-ReplicationScope "Forest" -MasterServers $destinationForest

#=================================ParaSimpsons=================================
#Obtenemos los parámetros del bosque remoto
$remoteContext = New-Object -TypeName "System.DirectoryServices.ActiveDirectory.DirectoryContext" `
-ArgumentList @( "Forest", $strRemoteForest, $strRemoteAdmin, $strRemoteAdminPassword)

#Creamos un objeto de tipo bosque
$remoteForest = [System.DirectoryServices.ActiveDirectory.Forest]::getForest($remoteContext)

#obtenemos la config del bosque local
$localforest=[System.DirectoryServices.ActiveDirectory.Forest]::getCurrentForest()

#Creamos la relación de confianza
$localForest.CreateTrustRelationship($remoteForest,"Bidirectional")
echo "CreateTrustRelationship: Succeeded for domain $($remoteForest)"

#
Get-ADTrust -Filter * -Credential TercerPlaneta.local
