﻿# Variables
$ipaddress = "10.0.18.2"
$dnsaddress = "10.0.18.1"
$machineName = "THUNDERASERVER"

# Desactivar Firewall
Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled False
netsh advfirewall Set allprofiles State Off 

# Creamos una excepción al protocolo ICMP para probar conectividad
netsh advfirewall firewall add rule name="Allow Ping" protocol=icmpv4 dir=in action=allow


#Configuración de IP y DNS 
Disable-NetAdapterBinding -Name Ethernet -ComponentID ms_tcpip6 -PassThru
New-NetIPAddress -InterfaceAlias Ethernet -IPAddress $ipaddress -AddressFamily IPv4 -PrefixLength 18
Set-DnsClientServerAddress -InterfaceAlias Ethernet -ServerAddresses $dnsaddress

#Configuración de nombre de pc

Rename-Computer –Newname $machineName -Restart