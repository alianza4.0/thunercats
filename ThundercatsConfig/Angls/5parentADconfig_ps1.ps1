﻿# Variables
$domainName = "Thundercats.local"
$domainNetbiosName = "THUNDER"

#===============================================================
#=======================InstalarRoles===========================
#Obtener los roles disponibles
# Get-WindowsFeature

#De la lista, instalamos el rol de Active Directory Domain Services y el de DNS
Install-WindowsFeature -Name AD-Domain-Services -IncludeManagementTools
Install-WindowsFeature DNS -IncludeManagementTools

#Para ver los comandos disponibles del AD
# Get-Command -Module ADDSDeployment

#========================ConfigBosqueSimpsons============================
#Script para configurar bosque Thudercats.local
Import-Module ADDSDeployment
Install-ADDSForest `
-CreateDnsDelegation:$false `
-DatabasePath "C:\Windows\NTDS" `
-Domainmode "Win2012R2" `
-DomainName $domainName `
-DomainNetbiosName $domainNetbiosName `
-ForestMode "Win2012" `
-InstallDns:$true `
-LogPath "C:\Windows\NTDS" `
-NoRebootOnCompletion:$false `
-SysvolPath "C:\Windows\SYSVOL" `
-Confirm:$false `
-SafeModeAdministratorPassword (ConvertTo-SecureString -AsPlainText "hola123.," -Force) `
-Force:$true

Get-DnsServerResourceRecord -ZoneName "Thundercats.local"