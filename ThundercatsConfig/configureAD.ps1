#Instalar Active Directory
install-windowsfeature AD-Domain-Services -IncludeManagementTools

#ipconfig
New-NetIPAddress -InterfaceAlias "Ethernet" -IPAddress 10.0.20.1 -PrefixLength 18 
Set-DnsClientServerAddress -InterfaceAlias "Ethernet" -ServerAddresses ("127.0.0.1")

#Crear bosque thundercats.local
Install-ADDSForest -CreateDnsDelegation:$false -DatabasePath "C:\Windows\NTDS" -DomainMode "Win2012R2" -DomainName "thundercats.local" -DomainNetbiosName "THUNDERCATS" -ForestMode "Win2012R2" -InstallDns:$true -LogPath "C:\Windows\NTDS" -NoRebootOnCompletion:$false -SysvolPath "C:\Windows\SYSVOL" -Force:$true