#config
$domainName = "Thundera.Thundercats.local"
$domainNetbiosName = "THUNDERCATS"

#===============================================================
#=======================InstalarRoles===========================
#Obtener los roles disponibles
# Get-WindowsFeature

#De la lista, instalamos el rol de Active Directory Domain Services y el de DNS
Install-WindowsFeature -Name AD-Domain-Services -IncludeManagementTools
Install-WindowsFeature DNS -IncludeManagementTools

#========================ConfigBosqueSimpsons============================
#Script para configurar bosque simpsons.local
Import-Module ADDSDeployment
install-Addsdomain `
-domaintype "childDomain" `
-parentdomainname "thundercats.local" `
-newdomainname "thundera"  `
-credential ( credential )  `
-NoRebootOnCompletion:$false

#Al iniciar la configuración, preguntará por la password de recuperación

echo "Configurado Thundera.local"