﻿#Script para generar archivo .rea, configurar tercerplaneta para ser una CA Subordinada e instalar utilerias necesarias.


################IMPORTANTE###################
# Antes de correr el script, se debe de contar con el .pem de la CA Root Linux dentro de tercerplaneta.
# Colocar la ruta absulouta al archivo .pem dentro del valor de $pahtPem
################I############################


$nombreMaquinaVirtual = "CA"

Invoke-Command  -VMName $nombreMaquinaVirtual -ScriptBlock {
    "Instalando tools..."
    Install-WindowsFeature -Name ADCS-Cert-Authority -IncludeManagementTools
    Install-WindowsFeature ADCS-Web-Enrollment

    "Agregando certificando de CA Root..."   
    $pathPem = "C:\Users\Administrator\Downloads\cacert.pem" # <CAMBIAR> Ruta donde está el .pem de la CA Root 
    Import-Certificate -FilePath $pathPem -CertStoreLocation 'Cert:\LocalMachine\Root' -Verbose
    "Generando CA Subordinada y el archivo .req ..."
    Install-AdcsCertificationAuthority -CAType EnterpriseSubordinateCA -CACommonName "CASubordinadaTest" -KeyLength 2048 -HashAlgorithmName SHA256 -CryptoProviderName "RSA#Microsoft Software Key Storage Provider" 
    #Después de obtener el .req, se le manda el archivo a la CA Root linux. Éste lo firma, y nos devuelve el .crt
    "Exito. Archivo .req Generado en C:"
}

################IMPORTANTE###################

# Después de la ejecución del script, el archivo .req está dentro de C:
# Pasar ese archivo a la CA Root para que lo firme. 
#############################################

<#
Este es el script que en teoría, debería de hacer la instalación del .crt para que se valide.
Por alguna extraña razón, crea el registro dentro de Certs:\CurrentUser\CA (donde se almaneca dicho certificado), pero no hace uso del registro creado.

$nombreMaquinaVirtual = "CA"

Invoke-Command  -VMName $nombreMaquinaVirtual -ScriptBlock {
    "Agregando certificado firmado por la CA..."
    $pathCert = "C:\Users\Administrator\Downloads\CA.tercerplaneta.local_CASubordinadaTest.crt" #Ruta donde está el certificado ya firmado <CAMBIAR>
    Import-Certificate -FilePath $pathCert -CertStoreLocation 'Cert:\CurrentUser\CA' -Verbose
    "Certificado importado con éxito"
}


Para ejecutar el siguiente script, se ha agregado con GUI el .crt al DC para su uso.
Instrucciones: 
1) Abrir Certification Authority entro de tercerplaneta.local
2) Dar click derecho dentro en el nombre de la CA Subordinada > All Tasks > Install CA Certificate y seleccionar nuestro archivo .crt y dar OK.
3) Aparecerá un warning y damos "OK". 
4) Ahora, seleccionamos de nuevo el nombre de nuestra CA Subordinada y daremos click en el botón verde de play, en el barra superior, para iniciar el servicio de CA.
5) Listo, hemos importado correctamente el certificado de nuestra CA Root Linux.
#>



