﻿#Script para crear la plantilla Alines y hacerla publica para ser emitida.
$nombreMaquinaVirtual = "CA"


Invoke-Command  -VMName $nombreMaquinaVirtual -ScriptBlock {
    "Creando plantilla Aliens..."
    $ConfigContext = ([ADSI]"LDAP://RootDSE").ConfigurationNamingContext 
    $ADSI = [ADSI]"LDAP://CN=Certificate Templates,CN=Public Key Services,CN=Services,$ConfigContext" 
    $templatePath = "LDAP://CN=Aliens,CN=Certificate Templates,CN=Public Key Services,CN=Services,CN=Configuration,DC=Thundercats,DC=local"

    #Estos son para los valores shidos
    $NewTempl = $ADSI.Create("pKICertificateTemplate", "CN=Aliens")  
    $NewTempl.put("distinguishedName","CN=Aliens,CN=Certificate Templates,CN=Public Key Services,CN=Services,$ConfigContext")
    $NewTempl.put("flags","131640")
    $NewTempl.put("displayName","Aliens")
    $NewTempl.put("revision","100")
    $NewTempl.put("pKIDefaultKeySpec","1")

    $NewTempl.SetInfo()

    $NewTempl.put("pKIMaxIssuingDepth","0")
    $NewTempl.put("pKICriticalExtensions","2.5.29.15")
    $NewTempl.put("pKIExtendedKeyUsage","1.3.6.1.4.1.311.10.3.4, 1.3.6.1.5.5.7.3.2")
    $NewTempl.put("pKIDefaultCSPs","1,Microsoft RSA SChannel Cryptographic Provider")
    $NewTempl.put("msPKI-RA-Signature","0")
    $NewTempl.put("msPKI-Enrollment-Flag","41")
    $NewTempl.put("msPKI-Private-Key-Flag","100925456")
    $NewTempl.put("msPKI-Certificate-Name-Flag","-2113929216") #2113929216 #1979711488
    $NewTempl.put("msPKI-Minimal-Key-Size","2048")
    $NewTempl.put("msPKI-Template-Schema-Version","2")
    $NewTempl.put("msPKI-Template-Minor-Revision","2") #2 #4
    $NewTempl.put("msPKI-Cert-Template-OID","1.3.6.1.4.1.311.21.8.14204780.2945395.5786293.11224635.12515457.116.6314294.1068 143")
    $NewTempl.put("msPKI-Certificate-Application-Policy","1.3.6.1.4.1.311.10.3.4, 1.3.6.1.5.5.7.3.2")

    $NewTempl.SetInfo()

    $WATempl = $ADSI.psbase.children | where {$_.displayName -match "Basic EFS"}

    $NewTempl.pKIKeyUsage = $WATempl.pKIKeyUsage
    $NewTempl.pKIExpirationPeriod = $WATempl.pKIExpirationPeriod
    $NewTempl.pKIOverlapPeriod = $WATempl.pKIOverlapPeriod
    $NewTempl.SetInfo()
    " "
    "Se creo el certificado Aliens con Exito."
    "Detalles del Certificado: "
    $NewTempl | select *

    " "
    "Agregando el template Aliens para la distrubución..."
    Add-CATemplate -Name "Aliens" -Force #Agrega el template creado
    "Éxito" 
     #certutil -setreg ca\CRLFlags -CRLF_REVCHECK_IGNORE_OFFLINE
}
