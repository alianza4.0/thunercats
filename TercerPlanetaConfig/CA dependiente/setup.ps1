﻿Install-WindowsFeature -Name ADCS-Cert-Authority -IncludeManagementTools
Install-WindowsFeature ADCS-Web-Enrollment
Install-AdcsCertificationAuthority -CAType EnterpriseSubordinateCA -CACommonName "CASubordinada" -KeyLength 2048 -HashAlgorithmName SHA256 -CryptoProviderName "RSA#Microsoft Software Key Storage Provider"


#Después de obtener el .req, se le manda el archivo a la CA Root linux.
#Éste lo firma, y nos devuelve el .crt y adicionalmente, tenemos que agregar el .pem de la CA Root para nuestra CA
#subordinada sepa quién es la CA Root.


#Se instala el .pem dentro de la CA Subordinada.
Import-Certificate -FilePath C:\try2\cacert.pem -CertStoreLocation 'Cert:\LocalMachine\Root' -Verbose


#Se agrega el .crt firmado por la CA Root dentro de la subordinada. 
certutil -installCert C:\certs\forwarder.forwarder.local_CASubordinada.crt