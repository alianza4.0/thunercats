#=================================================================
#======================ConfigInicialClienteFlanders=======================
#Configurar una IP estática
$ipaddress = "10.0.18.3"
$dnsaddress = "10.0.20.2"
$newComputerName = "thunderaw10"
# $DomainName = "thundera.Thundercats.local"
#Configuraciones para entrar al dominio
$userCore = "THUNDERCATS\Administrator"
$pass4UserCore = "hola123.,"


#############################################################################
# Automatizado
#Desabilitar ipv6
Disable-NetAdapterBinding -Name * -ComponentID ms_tcpip6 -PassThru

New-NetIPAddress -InterfaceAlias "Ethernet" -IPAddress $ipaddress -AddressFamily IPv4 -PrefixLength 18
Set-DnsClientServerAddress -InterfaceAlias "Ethernet" -ServerAddresses $dnsaddress

#Firewall config
netsh advfirewall Set allprofiles State Off 

#Creamos una excepción al protocolo ICMP para probar conectividad
netsh advfirewall firewall add rule name="Allow Ping" protocol=icmpv4 dir=in action=allow

#Para cambiar el nombre al equipo
# Rename-Computer -NewName $newComputerName -Restart

# Para añadir al dominio
Add-Computer -DomainName "Thundera.Thundercats.local" -Restart


echo "************************"
echo "************************"
